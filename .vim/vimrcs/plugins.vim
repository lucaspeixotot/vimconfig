call plug#begin('~/.vim/plugged')

" Elegant status line
Plug 'itchyny/lightline.vim'

" Elegant buftab line
Plug 'ap/vim-buftabline'

" Smart commenter
Plug 'scrooloose/nerdcommenter'

" Smart surround substitution i.e '({
Plug 'tpope/vim-surround'

" Smart nerd tree
Plug 'scrooloose/nerdtree'

" Open last files
Plug 'yegappan/mru'

" Git info
Plug 'tpope/vim-fugitive'

call plug#end()
