vimrc_file="$HOME/.vimrc"
vimrc_home="$HOME/.vim/"

if [ -f "$vimrc_file" ]
then
    mv $vimrc_file $HOME/.vimrc-backup
fi

if [ -d "$vimrc_home" ];
then
    mv $vimrc_home $HOME/.vim-backup/
fi

cp .vimrc $vimrc_file
cp .vim $HOME/ -r
